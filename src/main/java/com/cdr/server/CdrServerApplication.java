package com.cdr.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class CdrServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdrServerApplication.class, args);
	}
}
