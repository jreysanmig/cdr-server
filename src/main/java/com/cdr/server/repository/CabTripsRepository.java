package com.cdr.server.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cdr.server.model.CabTripCount;
import com.cdr.server.model.CabTripData;

public interface CabTripsRepository extends JpaRepository<CabTripData, Long>{

	@Query("SELECT new com.cdr.server.model.CabTripCount(DATE(pickup_datetime), medallion, COUNT(t)) "
			+ "FROM CabTripData t "
			+ "WHERE medallion IN :medallions AND DATE(pickup_datetime)=:pickupdate "
			+ "GROUP BY medallion ")
	List<CabTripCount> findCountByMedallionAndPickupDate(@Param("medallions") List<String> medallions, @Param("pickupdate") Date pickupDate);
	
}
