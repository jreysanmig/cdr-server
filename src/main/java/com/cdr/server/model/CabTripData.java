package com.cdr.server.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="CabTripData")
public class CabTripData {
	
	@Id
	private String medallion;
	
	@Column(name="hack_license")
	private String hackLicense;	

	@Column(name="vendor_id")
	private String vendorId;
	
	@Column(name="rate_code")
	private int rateCode;
	
	@Column(name="store_and_fwd_flag")
	private String storeAndFwdFlag;
	
	@Column(name="pickup_datetime")
	@Temporal(TemporalType.DATE)
	private Date pickupDateTime;
	
	@Column(name="dropoff_datetime")
	@Temporal(TemporalType.DATE)
	private Date dropoffDateTime;
	
	@Column(name="passenger_count")
	private int passengerCount;
	
	@Column(name="trip_time_in_secs")
	private int tripTimeInSecs;
	
	@Column(name="trip_distance")
	private double tripDistance;

	public String getMedallion() {
		return medallion;
	}

	public void setMedallion(String medallion) {
		this.medallion = medallion;
	}

	public String getHackLicense() {
		return hackLicense;
	}

	public void setHackLicense(String hackLicense) {
		this.hackLicense = hackLicense;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public int getRateCode() {
		return rateCode;
	}

	public void setRateCode(int rateCode) {
		this.rateCode = rateCode;
	}

	public String getStoreAndFwdFlag() {
		return storeAndFwdFlag;
	}

	public void setStoreAndFwdFlag(String storeAndFwdFlag) {
		this.storeAndFwdFlag = storeAndFwdFlag;
	}

	public Date getPickupDateTime() {
		return pickupDateTime;
	}

	public void setPickupDateTime(Date pickupDateTime) {
		this.pickupDateTime = pickupDateTime;
	}

	public Date getDropoffDateTime() {
		return dropoffDateTime;
	}

	public void setDropoffDateTime(Date dropoffDateTime) {
		this.dropoffDateTime = dropoffDateTime;
	}

	public int getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(int passengerCount) {
		this.passengerCount = passengerCount;
	}

	public int getTripTimeInSecs() {
		return tripTimeInSecs;
	}

	public void setTripTimeInSecs(int tripTimeInSecs) {
		this.tripTimeInSecs = tripTimeInSecs;
	}

	public double getTripDistance() {
		return tripDistance;
	}

	public void setTripDistance(double tripDistance) {
		this.tripDistance = tripDistance;
	}
	
}
