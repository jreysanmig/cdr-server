package com.cdr.server.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="CabTripCount")
public class CabTripCount {
	
	@Temporal(TemporalType.DATE)
	@Column(name="pickup_datetime")
	private Date pickupDateTime;
	@Id
	private String medallion;
	private long count;
	
	public CabTripCount(Date pickupDateTime, String medallion, long count) {
		this.pickupDateTime = pickupDateTime;
		this.medallion = medallion;
		this.count = count;
	}
	
	public Date getPickupDateTime() {
		return pickupDateTime;
	}

	public void setPickupDateTime(Date pickupDateTime) {
		this.pickupDateTime = pickupDateTime;
	}

	public String getMedallion() {
		return medallion;
	}
	public void setMedallion(String medallion) {
		this.medallion = medallion;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}

}
