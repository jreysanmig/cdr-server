package com.cdr.server.controller;

import java.util.Date;
import java.util.List;

import org.assertj.core.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cdr.server.model.CabTripCount;
import com.cdr.server.repository.CabTripsRepository;

@RestController
@RequestMapping("/trips")
public class CabTripController {

	@Autowired
	private CabTripsRepository cabTripsRepository;
	
	@Cacheable(value="cabtripcount")
	@GetMapping(value="/cab/{medallions}/pickup/{pickupDate}")
	public List<CabTripCount> countByMedallionAndPickupDate(@PathVariable List<String> medallions, @PathVariable String pickupDate) {
		return countByMedallionAndPickupDateCacheIgnore(medallions,pickupDate,null);
	}
	
	@CachePut(value="cabtripcount", unless="#cacheIgnore==null")
	@GetMapping(value="/cab/{medallions}/pickup/{pickupDate}/{cacheIgnore}")
	public List<CabTripCount> countByMedallionAndPickupDateCacheIgnore(@PathVariable List<String> medallions, @PathVariable String pickupDate, @PathVariable String cacheIgnore) {
		Date pickup = DateUtil.parse(pickupDate);
		return cabTripsRepository.findCountByMedallionAndPickupDate(medallions,pickup);
	}
		
	@CacheEvict(value="cabtripcount", allEntries=true)
	@GetMapping(value="/cache-clear")
	public String clearCache() {
		// clears cabtripcount cache
		return "Cache cleared";
	}
	
}
